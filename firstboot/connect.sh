#! /bin/sh

# Wrapper script for the git installation and ssh keyfile generation.
. ./install_bash
. ./install_git
. ./ssh_config

install_bash

install_repo_tools

create_keypair
if [ $? -eq 0 ]
then
	publish_key_bitbucket
	if [ $? -ne 0 ]
	then
		echo "Failed publication of public key to BitBucket!"
		exit 1
	fi
else
	echo "FAIL - Could not publish public key to bitbucket - keypair creation failed!"
	exit 1
fi
