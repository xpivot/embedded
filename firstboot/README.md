# README #

This is a brief script used to initially get the Omega2+ device connected to the Murtech software repositories.

### What is this repository for? ###

This contains a single shell script that will be uploaded to the device and executed.

### How do I get set up? ###

1. You must have git on your local machine and would not likely be seeing this if you dont.
2. Clone this repository on your local machine.
3. On your O2+ device create a work directory (>mkdir work; cd work).
4. Using the Onion web app on the device and specifically the Terminal app, upload the contents of this repository to the O2+ device in your work directory.
5. Make the shell script executable.
6. Execute the shell script.

### Contribution guidelines ###

Contributions aren't expected as this is a very special purpose repository.

### Who do I talk to? ###

Bill Scott
bill.scott@xpivotcorp.com
(858) 449-5575
