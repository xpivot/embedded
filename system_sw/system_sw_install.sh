#! /bin/bash
#
# Install or upgrade system software.
#
#
# The packages file is expected to be in this directory.

opkg update

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PACKAGES=$DIR/packages

while read -r line ; do
	if [ "$line" ] ; then
		echo $line | cut -c 1 | grep '#' > /dev/null
		if [ $? -eq 0 ] ; then
			continue
		fi	

		opkg upgrade $line
		if [ $? -ne 0 ] ; then
			opkg install $line
		fi	
	fi
done < $PACKAGES 
