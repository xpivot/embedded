#! /bin/bash
#
# This is a watchdog to be run by cron.
#
# Features:
#
#	1. Ensure 802.11b/g connectivity.  The O2+ devices have shown to be
#	unreliable with regard to connecting with "familiar" WiFi networks.
#
#	2. Ensure wave data process is running.  For some unknown reason the
#	process won't always start at boot time.
#
#	3. Ensure geo-location data process is running.  Presumably same core
#	issue as wave data acquisition.
#


# Initialize the display and clear it; set ToD
oled-exp -i > /dev/null
oled-exp -c > /dev/null
oled-exp dim on > /dev/null
DATE=$(date +%Y%m%d-%H:%M:%S)
oled-exp write "$DATE\n" > /dev/null

# Get the software version and add it to the display.
VER=$(awk -F= '/SW_VERSION/ { print $2 }' /etc/buoy/buoy.cfg)
oled-exp write "S/W version $VER\n\n" > /dev/null


#####################################################
# WiFi - 802.11 b or g network connectivity.
# Ensure we're on a "familiar" network.  Such networks are configured in
# file /etc/config/wireless
# It may be a good idea to test for success and retry if necessary.  How many times???
# TODO -
# There's a serious defect here.  See https://bitbucket.org/xpivot/embedded/issues/42
# If apcli0 is present, the dog moves on.  However it's presence does not guarantee connection
# to an IPv4 network.  There may only be an IPv6 entry, which is bad!  Re-write this to catch
# the scenario under which we are still not connected on IPv4.
ifconfig | grep apcli0 > /dev/null
if [ $? -ne 0 ] ; then
	oled-exp write "SSID: searching\n\n" > /dev/null
	oled-exp write "IP:...searching...\n\n" > /dev/null
	printf "%s - Not connected to any 802.11b/g networks; running wifimanager.\n" "$DATE"	
	/usr/bin/wifimanager
	result=$?
	if [ "$result" -ne 0 ] ; then
		printf "%s - wifimanager returned error code [ %s ]\n" "$DATE" "$result"
	fi
else
	wifi_ssid=$(awk -F' ' '/ApCliSsid/ { print $3 }' /etc/config/wireless | awk -F\' '{print $2}')
	oled-exp write "SSID: $wifi_ssid\n" > /dev/null
	ip_addr=$(ifconfig | grep apcli0 -A 1 | grep inet | awk '{print $2}' | awk -F: '{print $2}')
	oled-exp write "IP: $ip_addr\n\n" > /dev/null
fi


#####################################################
# Service dog.  This is a common function to be used across any number of services
# given they are a service that is controlled via a file in /etc/init.d/
#
# Assumption: Must be a service with control file in /etc/init.d/
# Parameters: (1) service name (file found in /etc/init.d/)

service_dog () {
	DIR_INIT=/etc/init.d
	SERVICE="$DIR_INIT"/$1
	DATE=$(date +%Y%m%d-%H:%M:%S)
	#printf "%s - service_dog received parameter - service [ %s ]\n" "$DATE" "$1"
	#printf "%s - SERVICE [ %s ]\n" "$DATE" "$SERVICE"
	BIN_APP=$(grep -m 1 BIN_APP $SERVICE | awk -F= '/BIN_APP/ { print $2 }')
	PID=$(pidof ${BIN_APP})
	if [ ! -n "$PID" ]; then
		printf "%s - Service [ %s ] is not running...starting now [ %s start ]\n" "$DATE" "$1" "$SERVICE"
		$("$SERVICE" start) > /dev/null 2>&1
	fi
}


#####################################################
# Wave data acquisition process.
#
# /etc/init.d/svs_603 contains configuration data, such as process name.
# Process name is in variable $BIN_APP
service_dog svs_603


#####################################################
# Geo-location process.
#
# /etc/init.d/buoy_gps contains configuration data, such as process name.
# Process name is in variable $BIN_APP
service_dog buoy_gps


